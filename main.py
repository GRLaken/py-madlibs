import os
import fnmatch


def mad_story(story_name):
    x= open("libtxts/"+story_name+".txt", "r")
    string = x.read()
    print("Starting "+story_name+" story")
    print("")
    chunks = string.split()
    madlib=""
    hero="TEMP"
    villian="TEMP"
    comment= False
    for y in chunks:
        if comment == True:
            pass
        elif y == "END":
            madlib  += "\n"
        elif y == "HERO":
            if hero == "TEMP":
                x=input("Name of the hero: ")
                hero = x
                madlib += (str(hero)+" ")
            else:
                madlib += (str(hero)+" ")
        elif y == "VILLIAN":
            if hero == "TEMP":
                x=input("Name of the villian: ")
                villian = x
                madlib += (str(vilian)+" ")
            else:
                madlib += (str(villian)+" ")
        elif y == "NOUN":
            x=input("A noun: ")
            madlib += (str(x)+" ")
        elif y == "PRONOUN":
            x=input("A pronoun: ")
            madlib += (str(x)+" ")
        elif y == "ADJECTIVE":
            x=input("An adjective: ")
            madlib += (str(x)+" ")
        elif y == "COMMENT":
            #Start of comment
            comment = True
        elif y == "ENDCOMMENT":
            #End of comment
            comment = False
        else:
            #Just append the normal word here
            madlib += (str(y)+" ")
    print(madlib)
    print("Story over!")

def picker(stories):
    while True:
        story_choice = input("Pick a number: ")
        try:
            if int(story_choice) <= len(stories):
                mad_story(stories[int(story_choice)])
        except:
           pass

def story_picker():
    stories=[]
    for root, dir, files in os.walk("./libtxts"):
        for items in fnmatch.filter(files, "*"):
            if items.endswith(".txt"):
                stories+= [items.strip(".txt")]
    story_index=0
    for story in stories:
        print(str(story_index)+" : "+story)
    picker(stories)

while True:
    action=input("Action: ")
    action=action.upper()

    if action == "HELP":
        print("")
        print("STORY : starts the story selector")
        print("HELP : show this message")
        print("QUIT : quits madlibs")
        print("")
    elif action == "STORY":
        story_picker()
    elif action == "QUIT":
        break
    else:
        print("")
        print("That action is invalid try 'help'")
        print("")
